import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { LoginComponent } from './mycomponents/login/login.component';
import { RegisterComponent } from './mycomponents/register/register.component';
import { FireworksComponent } from './mycomponents/fireworks/fireworks.component';
import { PortfolioComponent } from './mycomponents/portfolio/portfolio.component';

const routes: Routes = [{ path: 'login', component: LoginComponent },
{ path: 'register', component: RegisterComponent },
{ path: 'fireworks', component: FireworksComponent },
{ path: 'portfolio', component: PortfolioComponent, },];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
